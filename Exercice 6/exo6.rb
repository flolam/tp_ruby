
def addChar nom
  alpha = ("a".."z").to_a
  somme = 0
  nom.each_char do |l|
    somme += (alpha.index(l)+1)
  end
  somme
end


def poids nom, prenom
  #creation de l'alpha

  somme = 0

  # on recupere l'emplacement des lettre dans l'alpha et on ajoute 1 pour palier au 0 de l'array
  somme += addChar(nom)
  somme += addChar(prenom)

  # on mets le resultat en array via digit on addition, si c'est superieur a 9 alors on refait la meme
  somme.digits.sum > 9 ? somme.digits.sum.digits.sum : somme.digits.sum
end



puts poids"florent", "lambrigger"