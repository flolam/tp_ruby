# tp_ruby

# Dans le cadre du télétravail, il vous est demandé d’effectuer les exercices sui-vants en ruby
Les consignes sont :

 •Création d’un dépôt GIT sur framagit en public.
 
 •Chaque exercice sera compris dans un répertoire portant le nom de l’exercice.
 
 •Les commits seront pertinents suivant l’exercice à effectuer.
 
 •Les codes devront être commentés afin de comprendre ce qu’il fait et à quoi il sert.
 
# Exercice_1 :  ( ça c’est le nom de l’exercice :) )
  
 Création d’un tableau avec les chiffres de 1 à 100.  (2 méthodes possibles)
 Une fois créer, afficher chaque valeur.
 
 
# Exercice_2:    ( c’est toujours le nom de l’exercice :)  si si ) 
 Depuis le tableau de l’exercice 1, méthode simplifiée,  afficher les nombres compris entre 30 et 59.

# Exercice 3:   (je pense que là c’est compris :-D )
 
 Toujours depuis l’exercice_1  Créer un nouveau tableau avec les nombre paires du tableau de l’exercice_1 en utilisant la méthode «select» 
 
# Exercice 4 :  (vous êtes sûr ? ) :) 
 
 Ecrire une méthode qui prendra des paramètres ‘first_name’ et ‘age’A l’appel de la méthode, celle ci renverra : «Je m’apelle ‘first_name’ , j’ai ‘age’ans » Si age n’est pas précisé la chaine renvoi «Je m’apelle ‘first_name’ »