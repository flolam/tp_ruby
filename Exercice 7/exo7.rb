#liste de mot
list = [
    "chien",
    "chat",
    "poussin",
    "vache",
    "koala",
    "crevette",
    "lapin",
    "serpent",
    "phoque",
    "otarie",
    "requin",
    "loutre",
    "fouine",
    "singe",
    "aigle",
    "cheval",
    "chevre",
    "zebre",
    "panda",
    "ours",
    "boeuf"
]
#nombre de vie
vie = 6
#on random lew mots et on en choisit un
mot = list.shuffle[0].split('')

motReponse = []

#on creer la grille de reponse
mot.each { |n| motReponse.push('-') }

# tant qu'il y a une vie ou une lettre non trouvée on lance
while motReponse.include?('-') && vie > 0
  puts motReponse.join(' ')
  puts "choissisez une lettre"
  puts "vous avez #{vie} vie(s)"

  #recupere la lettre
  lettre = gets.chomp

  mot.each_with_index { |l,i |
    # si la lettre existe dans le mot alors la remplacer dans la grille reponse
    lettre == l ? motReponse[i] = mot[i] : 'jsp quoi mettre'
  }
  # on perd une vie a moins qu'il y a la lettre dans le mot
  vie-=1 unless mot.index(lettre)
end
# affichage du resultat
vie>0 ? (puts"bravo vous avez trouvé le mot") : (puts "oups ! le mot etait #{mot.join('')}")